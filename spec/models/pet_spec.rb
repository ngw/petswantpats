require 'rails_helper'

RSpec.describe Pet, type: :model do
  context 'when acting normally' do
    let(:dog) do
      FactoryGirl.build(:dog,
                        facility: FactoryGirl.create(:lida, state:
                                    FactoryGirl.create(:sardegna)),
                        pet_pictures: [
                          FactoryGirl.create(:marleys_pic),
                          FactoryGirl.create(:second_marleys_pic)
                        ])
    end

    it 'should alias pictures to pet_pictures' do
      expect(dog.pictures.count).to eq(dog.pet_pictures.count)
      dog.pet_pictures.last.destroy
      expect(dog.pet_pictures.count).to eq(dog.pictures.count)
      dog.pictures.last.destroy
      expect(dog.pictures.count).to eq(dog.pet_pictures.count)
    end
  end

  context 'when creating' do
    let(:lida) do
      FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
    end
    let(:dachsund)  { FactoryGirl.create(:dachsund) }
    let(:yorkshire) { FactoryGirl.create(:yorkshire) }
    let(:mixture_dachsund) do
      FactoryGirl.build(:marley_dachsund, breed: dachsund)
    end
    let(:mixture_yorkshire) do
      FactoryGirl.build(:marley_yorkshire, breed: yorkshire)
    end
    let(:dog) do
      FactoryGirl.build(:marley,
                         mixtures: [mixture_yorkshire, mixture_dachsund],
                         pet: FactoryGirl.build(
                           :dog,
                           pet_pictures: [FactoryGirl.create(:marleys_pic),
                                          FactoryGirl.create(:second_marleys_pic)],
                           facility: lida)).decorate
    end

    it 'should be valid' do
      dog.save
      expect(dog).to be_valid
    end

    [:name, :birth, :facility].each do |attribute|
      it "should have a #{attribute}" do
        dog.pet.send("#{attribute}=", nil)
        dog.save
        expect(dog.pet).not_to be_valid
        expect(dog.pet.errors[attribute].count).to eq(1)
      end
    end

    it 'should have a valid color' do
      dog.pet.color = 26
      dog.save
      expect(dog.pet).not_to be_valid
      expect(dog.pet.errors[:color].count).to eq(1)
    end
  end

  context 'when searching' do
    it 'should cleanup from empty params' do
      expect(Pet.search_params(a: '', b: nil)).to eq(nil)
    end

    it 'should return q' do
      expect(Pet.search_params(q: 'test')).to eq('test')
    end

    it 'should search 1 year range' do
      birth_hash = Pet.birth_range(1)
      expect(birth_hash[:lte]).to be <= 1.years.ago
      expect(birth_hash[:gte]).to be <= 2.years.ago
    end

    it 'should search for correct weight range' do
      weight_hash = Pet.weight_range('20-30')
      expect(weight_hash[:lte]).to eq(30)
      expect(weight_hash[:gte]).to eq(20)
    end
  end
end
