require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.create(:johndoe) }
  let(:facility) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end

  context 'when creating' do
    [:email, :username].each do |attribute|
      it "should have a #{attribute}" do
        user.send("#{attribute}=", nil)
        expect(user).not_to be_valid
        expect(user.errors[attribute].count).to eq(1)
      end
    end
  end

  context 'when managing facilities' do
    it 'should not have any facility to manage' do
      expect(user.facilities).to be_empty
    end

    it 'should have a facility to manage' do
      user.add_role(:owner, facility)
      expect(user.facilities).not_to be_empty
      expect(user.facilities.count).to eq(1)
    end
  end
end
