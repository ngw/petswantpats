require 'rails_helper'

RSpec.describe PetPicture, type: :model do
  let(:lida) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end
  let(:dachsund)  { FactoryGirl.create(:dachsund) }
  let(:yorkshire) { FactoryGirl.create(:yorkshire) }
  let(:mixture_dachsund) do
    FactoryGirl.build(:marley_dachsund, breed: dachsund)
  end
  let(:mixture_yorkshire) do
    FactoryGirl.build(:marley_yorkshire, breed: yorkshire)
  end
  let(:dog) do
    FactoryGirl.create(:dog, facility: lida,
                       pictures: [FactoryGirl.create(:marleys_pic),
                                 FactoryGirl.create(:second_marleys_pic)],
                       animal: FactoryGirl.create(:marley,
                                                  mixtures: [mixture_yorkshire, mixture_dachsund]))
  end

  context 'when creating' do
    it 'should be valid' do
      dog.pictures.each do |picture|
        expect(picture).to be_valid
      end
    end
  end

  context 'when setting main picture' do
    it 'should have at least one' do
      expect(PetPicture.where(
        ['pet_id = :pet_id AND main = :main', { pet_id: dog.id, main: true }])
      ).not_to be_empty
    end

    it 'should have only one' do
      first = dog.pictures[0]
      second = dog.pictures[1]
      expect(first).to be_main
      expect(second).not_to be_main
      second.update_attribute(:main, true)
      expect(PetPicture.find(first.id)).not_to be_main
      expect(second).to be_main
    end
  end
end
