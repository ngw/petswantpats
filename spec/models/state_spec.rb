require 'rails_helper'

RSpec.describe State, type: :model do
  let(:state) { FactoryGirl.create(:sardegna) }
  let(:facility) { FactoryGirl.create(:lida, state: state) }

  context 'when creating' do
    it 'should be valid' do
      expect(state).to be_valid
    end

    it 'should have a name' do
      state.name = nil
      expect(state).not_to be_valid
      expect(state.errors[:name].count).to eq(1)
    end

    it 'should fetch states with facilities' do
      expect(facility).to be_valid
      expect(State.that_has_facilities.count).to eq(1)
    end
  end
end
