require 'rails_helper'

RSpec.describe Breed, type: :model do
  context 'when creating' do
    let(:dachsund) { FactoryGirl.create(:dachsund) }

    it 'should have a name' do
      dachsund.name = nil
      expect(dachsund).not_to be_valid
      expect(dachsund.errors[:name].count).to eq(1)
    end

    it 'should have a unique name' do
      expect(dachsund).to be_valid
      second_dachsund = FactoryGirl.build(:dachsund)
      expect(second_dachsund.save).to eq(false)
      expect(second_dachsund.errors[:name].count).to eq(1)
    end
  end
end
