require 'rails_helper'

RSpec.describe Facility, type: :model do
  let(:facility) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end

  context 'when creating' do
    it 'should be valid' do
      expect(facility).to be_valid
    end

    [
      :name, :address, :postal_code, :city, :province, :state
    ].each do |attribute|
      it "should have a #{attribute}" do
        facility.send("#{attribute}=", nil)
        allow(facility).to receive(:full_address
                                  ).and_return('Loc. Colcò, 07026 Olbia (OT)')
        expect(facility).not_to be_valid
        expect(facility.errors[attribute].count).to eq(1)
      end
    end

    it 'can have a logo' do
      facility.logo = Rack::Test::UploadedFile.new(
        File.open(File.join(Rails.root, '/spec/fixtures/example_logo.png')))
      expect(facility.save).to be_truthy
      expect(facility.logo).not_to be_nil
    end

    it 'should have a slug' do
      expect(facility.slug).to eq('sardegna-olbia-rifugio-i-fratelli-minori')
    end
  end

  context 'when geocoding' do
    it 'has a full address' do
      expect(facility.full_address).to eq('Loc. Colcò, 07026 Olbia (OT)')
    end

    it 'gets coordinates' do
      expect(facility).to be_valid
      expect(facility.latitude).to eq(10.010101)
      expect(facility.longitude).to eq(-12.121212)
    end
  end

  context 'when instantiating a pet' do
    let(:pet) { facility.construct_pet(:dog) }

    it 'creates an empty Dog object' do
      expect(pet.animal.is_a?(Dog)).to be_truthy
    end

    it 'creates an empty array of pictures' do
      expect(pet.pictures).not_to be_nil
      expect(pet.pictures.count).to eq(0)
    end

    it 'creates and empty array of mixtures' do
      expect(pet.animal.mixtures).not_to be_nil
      expect(pet.animal.mixtures.count).to eq(0)
    end
  end
end
