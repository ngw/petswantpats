require 'rails_helper'

RSpec.describe Mixture, type: :model do
  context 'when creating' do
    let(:lida) do
      FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
    end
    let(:dachsund)  { FactoryGirl.create(:dachsund) }
    let(:yorkshire) { FactoryGirl.create(:yorkshire) }
    let(:mixture_dachsund) do
      FactoryGirl.build(:marley_dachsund, breed: dachsund)
    end
    let(:mixture_yorkshire) do
      FactoryGirl.build(:marley_yorkshire, breed: yorkshire)
    end
    let(:dog) do
      FactoryGirl.build(:marley,
                         mixtures: [mixture_yorkshire, mixture_dachsund],
                         pet: FactoryGirl.build(
                           :dog,
                           pet_pictures: [FactoryGirl.create(:marleys_pic),
                                          FactoryGirl.create(:second_marleys_pic)],
                           facility: lida)).decorate
    end

    before(:each) { dog.save }

    it 'should be valid' do
      expect(mixture_dachsund).to be_valid
      expect(mixture_yorkshire).to be_valid
    end

    it 'should have a breed' do
      mixture_dachsund.breed = nil
      expect(mixture_dachsund).not_to be_valid
      expect(mixture_dachsund.errors[:breed]).not_to be_empty
    end

    it 'should be unique' do
      expect(mixture_dachsund).to be_valid
      second_mixture_dachsund = FactoryGirl.build(
        :marley_dachsund, breed: dachsund, blended: dog)
      expect(second_mixture_dachsund).not_to be_valid
      expect(second_mixture_dachsund.errors[:breed]).not_to be_empty
    end

    it 'should not be more than 100%' do
      expect(mixture_dachsund).to be_valid
      expect(mixture_yorkshire).to be_valid
      mixture_yorkshire.percentage = 80
      expect(mixture_yorkshire.save).to eq(false)
      expect(mixture_yorkshire).not_to be_valid
      expect(mixture_yorkshire.errors[:percentage]
            ).to eq([
              'la somma delle percentuali è 155% ma deve essere minore di 100%'
            ])
    end

    it 'should handle "input_name" to create and normalize new breed' do
      mixture_dachsund.input_name = 'German Sheperd'
      expect(mixture_dachsund.save).to be_truthy
      expect(Breed.last.name).to eq('German sheperd')
    end
  end
end
