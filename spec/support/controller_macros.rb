module ControllerMacros
  def login_johndoe
    before(:each) do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      johndoe = FactoryGirl.create(:johndoe)
      sign_in johndoe
    end
  end
end
