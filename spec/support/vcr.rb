VCR.configure do |c|
  c.cassette_library_dir = Rails.root + 'spec/fixtures/cassettes'
  c.hook_into :webmock
end
