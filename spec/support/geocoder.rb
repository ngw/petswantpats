Geocoder.configure(lookup: :test)

Geocoder::Lookup::Test.add_stub(
  'Loc. Colcò, 07026 Olbia (OT)', [{
    latitude: 10.010101,
    longitude: -12.121212,
    address: 'Olbia, OT, Italy',
    state: 'Olbia',
    state_code: '',
    country: 'Italy',
    country_code: 'IT' }])

Geocoder::Lookup::Test.add_stub(
  'Whatever road, 07026 Test city (AA)', [{
    latitude: 12.010101,
    longitude: -10.121212,
    address: 'Test city, AA, Italy',
    state: 'AAAgrigento',
    state_code: '',
    country: 'Italy',
    country_code: 'IT' }])
