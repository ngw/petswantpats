require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  let(:johndoe) { FactoryGirl.create(:johndoe) }

  context 'when sending' do
    before :each do
      UserMailer.ask_access(johndoe, 'Test').deliver_now
      @email = ActionMailer::Base.deliveries.first
    end

    it 'should be sent by the right person' do
      expect(@email.from.first).to eq(johndoe.email)
    end

    it 'should have the right subject' do
      subject = "Richiesta di accesso privilegiato da #{johndoe.email}."
      expect(@email.subject).to eq(subject)
    end
  end
end
