FactoryGirl.define do
  factory :johndoe, class: User do
    email 'johndoe@example.com'
    username 'johndoe'
    password 'testpass'
    confirmed_at Time.now
  end

  factory :toddneal, class: User do
    email 'toddneal@example.com'
    username 'toddneal'
    password 'testpass'
    confirmed_at Time.now
  end
end
