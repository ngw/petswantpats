FactoryGirl.define do
  factory :dachsund, class: Breed do
    name 'Dachsund'
  end

  factory :yorkshire, class: Breed do
    name 'Yorkshire'
  end
end
