FactoryGirl.define do
  factory :marleys_pic, class: PetPicture do
    caption "Someone's having fun"
    main true
    photo_file Rack::Test::UploadedFile.new(
      File.open(File.join(Rails.root, '/spec/fixtures/example.jpg')))
  end

  factory :second_marleys_pic, class: PetPicture do
    caption 'More fun here!'
    main false
    photo_file Rack::Test::UploadedFile.new(
      File.open(File.join(Rails.root, '/spec/fixtures/example.jpg')))
  end
end
