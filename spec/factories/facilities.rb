FactoryGirl.define do
  factory :lida, class: Facility do
    name 'Rifugio "I Fratelli Minori"'
    address 'Loc. Colcò'
    postal_code '07026'
    contact_email 'test@example.com'
    city 'Olbia'
    province 'OT'
  end
end
