FactoryGirl.define do
  factory :marley, class: Dog do
    vaccinated true
    dewormed true
    spayed false
  end
end
