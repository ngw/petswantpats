FactoryGirl.define do
  factory :marley_dachsund, class: Mixture do
    percentage 75
  end

  factory :marley_yorkshire, class: Mixture do
    percentage 25
  end
end
