FactoryGirl.define do
  factory :dog, class: Pet do
    name 'Marley'
    birth 1.year.ago
    gender true
    color 0
  end
end
