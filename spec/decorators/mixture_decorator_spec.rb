require 'spec_helper'

describe MixtureDecorator do
  let(:yorkshire) { FactoryGirl.create(:yorkshire) }
  let(:lida) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end
  let(:dog) do
    FactoryGirl.build( :dog, facility: lida,
                        pictures: [FactoryGirl.create(:marleys_pic),
                                   FactoryGirl.create(:second_marleys_pic)],
                        animal: FactoryGirl.build(:marley)
                      ).decorate
  end
  let(:mixture_yorkshire) do
    FactoryGirl.build(:marley_yorkshire, breed: yorkshire, blended: dog.animal).decorate
  end

  it 'returns a percentage' do
    expect(mixture_yorkshire.percentage).to eq('25%')
  end
end
