require 'spec_helper'

describe StateDecorator do
  let(:state) { FactoryGirl.create(:sardegna).decorate }
  let(:facility) { FactoryGirl.create(:lida, state: state) }

  it 'returns the name of the state with the number of facilities' do
    expect(facility).to be_valid
    expect(state.name_with_facilities_number).to eq('Sardegna (1)')
  end
end
