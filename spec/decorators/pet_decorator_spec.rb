require 'spec_helper'

describe PetDecorator do
  let(:pet) { FactoryGirl.build(:dog).decorate }

  it 'returns a readable color' do
    expect(pet.color).to eq('crema')
  end

  it 'returns a readable gender' do
    expect(pet.gender).to eq('femmina')
  end
end
