require 'spec_helper'

describe DogDecorator do
  let(:lida) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end
  let(:dachsund) { FactoryGirl.create(:dachsund) }
  let(:yorkshire) { FactoryGirl.create(:yorkshire) }
  let(:mixture_dachsund) do
    FactoryGirl.build(:marley_dachsund, breed: dachsund)
  end
  let(:mixture_yorkshire) do
    FactoryGirl.build(:marley_yorkshire, breed: yorkshire)
  end
  let(:dog) do
    FactoryGirl.create( :dog, facility: lida,
                        pictures: [FactoryGirl.create(:marleys_pic),
                                   FactoryGirl.create(:second_marleys_pic)],
                        animal: FactoryGirl.create(:marley,
                                                   mixtures: [mixture_yorkshire, mixture_dachsund]
                      ).decorate).decorate
  end

  [:vaccinated, :spayed, :dewormed].each do |attribute|
    it "returns a readable #{attribute} attribute" do
      expect(dog.animal.send(attribute)).to match(/si|no/)
    end
  end

  it 'returns a readable mixture string' do
    expect(mixture_dachsund).to be_valid
    expect(mixture_yorkshire).to be_valid
    expect(dog.animal.breed).to eq('Yorkshire 25% - Dachsund 75%')
  end

  it 'returns a more simple readable mixture string' do
    expect(mixture_dachsund).to be_valid
    expect(mixture_yorkshire).to be_valid
    expect(dog.animal.simple_breed).to eq('Yorkshire - Dachsund')
  end
end
