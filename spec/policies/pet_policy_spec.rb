require 'rails_helper'

describe PetPolicy do
  subject { described_class }
  let(:johndoe) { FactoryGirl.create(:johndoe) }
  let(:lida) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end

  permissions :new?, :create?, :destroy? do
    it 'denies access if user is not a facility owner' do
      expect(subject).not_to permit(johndoe, Pet.new(facility: lida))
    end

    it 'grants access if user is a facility owner' do
      johndoe.add_role(:owner, lida)
      expect(subject).to permit(johndoe, Pet.new(facility: lida))
    end
  end
end
