require 'rails_helper'

describe FacilityPolicy do
  subject { described_class }
  let(:johndoe) { FactoryGirl.create(:johndoe) }
  let(:facility) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end

  permissions :new?, :create?, :destroy? do
    it 'rejects access if user is not an admin' do
      expect(subject).not_to permit(johndoe, facility)
    end

    it 'grants access if user is an admin' do
      johndoe.add_role(:admin)
      expect(subject).to permit(johndoe, facility)
    end
  end

  permissions :edit?, :update? do
    it 'rejects access if user is not the owner of the facility' do
      expect(subject).not_to permit(johndoe, facility)
    end

    it 'grants access if user is owner of the facility' do
      johndoe.add_role(:owner, facility)
      expect(subject).to permit(johndoe, facility)
    end
  end
end
