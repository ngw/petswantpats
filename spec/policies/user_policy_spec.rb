require 'rails_helper'

describe UserPolicy do
  subject { described_class }
  let(:johndoe) { FactoryGirl.create(:johndoe) }
  let(:toddneal) { FactoryGirl.create(:toddneal) }

  permissions :update?, :edit? do
    it 'denies access if user is not the same' do
      expect(subject).not_to permit(johndoe, toddneal)
    end

    it 'grants access if user is an admin' do
      johndoe.add_role(:admin)
      expect(subject).to permit(johndoe, toddneal)
    end

    it 'grants access if user is changing his own profile' do
      expect(subject).to permit(johndoe, johndoe)
    end
  end
end
