require 'rails_helper'

RSpec.describe PetsController, type: :controller do
  subject { described_class }
  login_johndoe
  let(:johndoe) { User.find_by(slug: 'johndoe') }
  let(:lida) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end
  let(:dachsund) { FactoryGirl.create(:dachsund) }
  let(:yorkshire) { FactoryGirl.create(:yorkshire) }
  let(:mixture_dachsund) do
    FactoryGirl.build(:marley_dachsund, breed: dachsund)
  end
  let(:mixture_yorkshire) do
    FactoryGirl.build(:marley_yorkshire, breed: yorkshire)
  end
  let(:dog) do
    FactoryGirl.create( :dog, facility: lida,
                        pictures: [FactoryGirl.create(:marleys_pic),
                                   FactoryGirl.create(:second_marleys_pic)],
                        animal: FactoryGirl.create(:marley,
                                                   mixtures: [mixture_yorkshire, mixture_dachsund])
                      ).decorate
  end

  context 'instantiating' do
    it 'should not be available to all users' do
      expect do
        get :new, facility_id: lida.slug
      end.to raise_error(Pundit::NotAuthorizedError)
    end

    it 'should assign a pet to a facility' do
      johndoe.add_role(:owner, lida)
      get :new, facility_id: lida.slug
      expect(assigns(:facility)).to eq(lida)
      expect(assigns(:pet).facility).to eq(lida)
    end

    it 'should be instantiated correctly' do
      johndoe.add_role(:owner, lida)
      get :new, facility_id: lida.slug
      expect(assigns(:pet).animal_type).to eq('Dog')
    end
  end

  context 'creating' do
    it 'should not be available to all users' do
      expect do
        post :create, facility_id: lida.slug,
                      pet: { name: 'test', birth: 1.year.ago, color: 10, weight: 10,
                             animal_attributes: { fur_size: 1, spayed: true, dewormed: true,
                                                  microchipped: false, vaccinated: false } }
      end.to raise_error(Pundit::NotAuthorizedError)
    end

    it 'should create a pet' do
      johndoe.add_role(:owner, lida)
      post :create, facility_id: lida.slug,
                    pet: { name: 'test', birth: 1.year.ago, color: 10, weight: 10,
                           pet_pictures_attributes: [
                             photo_file: Rack::Test::UploadedFile.new(
                               File.open(File.join(Rails.root, '/spec/fixtures/example.jpg')))],
                           animal_attributes: { fur_size: 1, spayed: true, dewormed: true,
                                                microchipped: false, vaccinated: false,
                                                mixtures_attributes: [
                                                  input_name: 'Breed', percentage: '35'] } }
      expect(response).to redirect_to(
        facility_pet_path(facility_id: assigns(:pet).facility.slug,
                          id: assigns(:pet).slug))
    end

    it 'should not create a pet and render the new action again' do
      johndoe.add_role(:owner, lida)
      post :create, facility_id: lida.slug,
                    pet: { name: '', birth: 1.year.ago, color: 10, weight: 10,
                           pet_pictures_attributes: [
                             photo_file: Rack::Test::UploadedFile.new(
                               File.open(File.join(Rails.root, '/spec/fixtures/example.jpg')))],
                           animal_attributes: { fur_size: 1, spayed: true, dewormed: true,
                                                microchipped: false, vaccinated: false,
                                                mixtures_attributes: [
                                                  input_name: 'Breed', percentage: '35'] } }
      expect(response).not_to redirect_to(
        facility_pet_path(
          facility_id: assigns(:pet).facility.slug,
          id: assigns(:pet).slug))
      expect(response).to render_template(:new)
    end
  end

  context 'editing' do
    it 'should not be available to all users' do
      expect do
        get :edit, facility_id: lida.slug, id: dog.pet.slug
      end.to raise_error(Pundit::NotAuthorizedError)
    end

    it 'should update a pet' do
      johndoe.add_role(:owner, lida)
      put :update, facility_id: lida.slug, id: dog.pet.slug,
                   pet: { name: 'test', birth: 1.year.ago, color: 10, weight: 10,
                          pet_pictures_attributes: [
                            photo_file: Rack::Test::UploadedFile.new(
                              File.open(File.join(Rails.root, '/spec/fixtures/example.jpg')))],
                          animal_attributes: { fur_size: 1, spayed: true, dewormed: true,
                                               microchipped: false, vaccinated: false,
                                               mixtures_attributes: [
                                                 input_name: 'Breed', percentage: '35'] } }
      expect(response.response_code).to redirect_to(
        facility_pet_path(facility_id: assigns(:pet).facility.slug,
                          id: assigns(:pet).slug))
      pet = Pet.friendly.find(dog.pet.slug)
      expect(pet.name).to eq('test')
      expect(pet.animal.spayed).to be_truthy
      expect(pet.animal.vaccinated).to be_falsy
    end

    it 'should not update a pet and render the edit action again' do
      johndoe.add_role(:owner, lida)
      put :update, facility_id: lida.slug, id: dog.pet.slug,
                   pet: { name: '', birth: 1.year.ago, color: 10, weight: 10,
                          pet_pictures_attributes: [
                            photo_file: Rack::Test::UploadedFile.new(
                              File.open(File.join(Rails.root, '/spec/fixtures/example.jpg')))],
                          animal_attributes: { fur_size: 1, spayed: true, dewormed: true,
                                               microchipped: false, vaccinated: false,
                                               mixtures_attributes: [
                                                 input_name: 'Breed', percentage: '35'] } }
      expect(response).not_to redirect_to(
        facility_pet_path(facility_id: assigns(:pet).facility.slug,
                          id: assigns(:pet).slug))
      expect(response).to render_template(:edit)
      pet = Pet.friendly.find(dog.pet.slug)
      expect(pet.name).to eq('Marley')
    end
  end

  context 'getting a dog' do
    it 'should be decorated' do
      get :show, facility_id: dog.pet.facility.slug, id: dog.pet.slug
      expect(assigns(:dog)).to be_decorated
    end
  end
end
