require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  subject { described_class }
  login_johndoe
  let(:toddneal) { FactoryGirl.create(:toddneal) }
  let(:johndoe) { User.find_by(slug: 'johndoe') }

  context 'editing' do
    it 'should have a user set' do
      get :edit, id: johndoe.slug
      expect(assigns(:user)).to eq(User.find_by(slug: 'johndoe'))
    end

    it 'should not be permitted to another user' do
      expect do
        get :edit, id: toddneal.slug
      end.to raise_error(Pundit::NotAuthorizedError)
    end
  end

  context 'updating' do
    it 'should not be permitted to another user' do
      expect do
        put :update, id: toddneal.slug
      end.to raise_error(Pundit::NotAuthorizedError)
    end

    it 'should update a profile' do
      put :update, id: johndoe.slug, user: {
        first_name: 'Manny', last_name: 'Milo', gender: true }
      user = User.find_by(slug: johndoe.slug)
      expect(user.first_name).to eq('Manny')
      expect(user.last_name).to eq('Milo')
      expect(user.gender).to be_truthy
    end
  end
end
