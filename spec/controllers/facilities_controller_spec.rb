require 'rails_helper'

RSpec.describe FacilitiesController, type: :controller do
  login_johndoe
  let(:johndoe) { User.find_by(slug: 'johndoe') }
  let(:lida) do
    FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
  end

  context 'lokking at a single facility' do
    it 'should be successful' do
      get :show, id: lida.slug
      expect(response).to be_success
    end

    it 'should find the right facility' do
      get :show, id: lida.slug
      expect(assigns(:facility)).to eq(lida)
    end
  end

  context 'updating' do
    it 'should not be permitted to external users' do
      expect do
        put :update, id: lida.slug
      end.to raise_error(Pundit::NotAuthorizedError)
    end

    it 'should update a facility' do
      johndoe.add_role(:owner, lida)
      put :update, id: lida.slug, facility: {
        name: 'Test updated facility', address: 'Whatever road',
        city: 'Test city', province: 'AA', telephone: '777777777' }
      facility = Facility.find_by(slug: lida.slug)
      expect(facility.name).to eq('Test updated facility')
      expect(facility.address).to eq('Whatever road')
      expect(facility.city).to eq('Test city')
      expect(facility.province).to eq('AA')
      expect(facility.telephone).to eq('777777777')
    end
  end
end
