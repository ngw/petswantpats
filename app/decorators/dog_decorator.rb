class DogDecorator < Draper::Decorator
  delegate_all
  decorates_association :pet
  decorates_associations :mixtures

  [:vaccinated, :dewormed, :spayed, :microchipped].each do |attribute|
    define_method attribute do
      I18n.translate(:booleans)[object.send(attribute)]
    end
  end

  def breed
    object.mixtures.collect do |mixture|
      "#{mixture.breed_name} #{mixture.percentage}%"
    end.join(' - ')
  end

  def simple_breed
    object.mixtures.collect do |mixture|
      "#{mixture.breed_name}"
    end.join(' - ')
  end

  def gender
    I18n.translate(:genders)[object.pet_gender]
  end

  def fur_length
    I18n.translate(:fur_length)[object.fur_length]
  end

  def size_at_maturity
    I18n.translate(:size_at_maturity)[object.size_at_maturity]
  end

  def color
    I18n.translate(:colors)[object.pet_color]
  end
end
