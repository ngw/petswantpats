class StateDecorator < Draper::Decorator
  delegate_all

  def name_with_facilities_number
    "#{object.name} (#{object.facilities.count})"
  end
end
