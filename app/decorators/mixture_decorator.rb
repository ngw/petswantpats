class MixtureDecorator < Draper::Decorator
  delegate_all

  def percentage
    "#{object.percentage}%"
  end
end
