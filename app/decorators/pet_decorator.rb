class PetDecorator < Draper::Decorator
  delegate_all

  def gender
    I18n.translate(:genders)[object.gender]
  end

  def color
    I18n.translate(:colors)[object.color]
  end
end
