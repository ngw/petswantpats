class FacilitiesController < ApplicationController
  before_action :authenticate_user!, except: :show
  after_action :verify_authorized, except: :show

  def show
    @facility = Facility.friendly.find(params[:id]).decorate
    @pets = @facility.pets.page(params[:page])
    respond_to do |format|
      format.html
      format.js { render 'pets/pets', layout: false }
    end
  end

  def edit
    @facility = Facility.friendly.find(params[:id])
    authorize @facility
    @pets = @facility.pets.page(params[:page])
  end

  def update
    @facility = Facility.friendly.find(params[:id])
    authorize @facility
    if @facility.update_attributes(secure_params)
      redirect_to facility_path(@facility.slug), notice: 'Struttura aggiornata'
    else
      redirect_to edit_user_registration_path
    end
  end

  protected

  def secure_params
    params.require(:facility).permit(
      [:logo, :name, :description, :telephone, :address,
       :postal_code, :city, :province, :state_id])
  end
end
