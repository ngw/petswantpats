class PetsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show, :search, :advanced_search_form]
  after_action :verify_authorized, except: [:index, :show, :search, :advanced_search_form, :adopt]

  def show
    @dog = Pet.friendly.find(params[:id]).animal.decorate
  end

  def search
    @response = Pet.search(Pet.search_params(params))
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @facility = Facility.friendly.find(params[:facility_id])
    @pet = @facility.construct_pet(:dog)
    authorize @pet
  end

  def create
    @facility = Facility.friendly.find(params[:facility_id])
    @pet = @facility.pets.create(secure_params)
    authorize @pet
    if @pet.valid?
      redirect_to facility_pet_path(
        facility_id: @pet.facility.slug, id: @pet.slug)
    else
      render :new, facility_id: @facility.slug
    end
  end

  def edit
    @facility = Facility.friendly.find(params[:facility_id])
    @pet = Pet.friendly.find(params[:id])
    authorize @pet
  end

  def update
    @facility = Facility.friendly.find(params[:facility_id])
    @pet = Pet.friendly.find(params[:id])
    authorize @pet
    if @pet.update_attributes(secure_params)
      redirect_to facility_pet_path(
        facility_id: @pet.facility.slug, id: @pet.slug)
    else
      render :edit, facility_id: @facility.slug, id: @pet.slug
    end
  end

  def adopt
    facility = Facility.friendly.find(params[:facility_id])
    pet = Pet.friendly.find(params[:pet_id])
    UserMailer.adopt(facility, pet, current_user, params[:adopt][:body]).deliver_now
    flash[:notice] = I18n.translate('adopt_message').capitalize
    redirect_to facility_pet_path(facility_id: facility.slug, id: pet.slug)
  end

  private

  def secure_params
    params.require(:pet).permit([
      :name, :description, :birth, :gender, :color, :weight, :facility_id,
      :heart, pet_pictures_attributes: [:photo_file, :caption, :id, :_destroy],
              animal_attributes: [
                :spayed, :dewormed, :microchipped, :vaccinated, :fur_length, :id, :size_at_maturity,
                mixtures_attributes: [:input_name, :percentage, :id, :_destroy]]])
  end
end
