class StatesController < ApplicationController
  def show
    @state = State.friendly.find(params[:id]).decorate
  end
end
