module Users
  class CallbacksController < ApplicationController
    def facebook
      @user = User.from_omniauth(request.env['omniauth.auth'])
      if @user.persisted?
        sign_in_and_redirect @user, event: :authentication
        flash[:notice] = 'Ti sei autenticato con successo'
      else
        render
      end
    end

    def failure
      flash[:error] = 'Autorizzazione non riuscita, ritenta.'
      redirect_to new_registration_path
    end
  end
end
