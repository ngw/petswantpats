module Users
  class RegistrationsController < Devise::RegistrationsController
    protected

    def sign_up_params
      params.require(:user).permit([
        :username, :avatar, :uid, :provider, :email, :first_name,
        :last_name, :location, :state_id, :gender, :birthday, :password,
        :password_confirmation
      ])
    end

    def after_inactive_sign_up_path_for(_resource)
      '/users/signed_up'
    end
  end
end
