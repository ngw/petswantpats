class UsersController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized

  def edit
    @user = User.friendly.find(params[:id])
    authorize @user
  end

  def update
    @user = User.friendly.find(params[:id])
    authorize @user
    if @user.update_attributes(secure_params)
      redirect_to root_path, notice: I18n.t('devise.registrations.updated')
    else
      redirect_to edit_user_registration_path
    end
  end

  def ask_access
    @user = User.friendly.find(params[:user_id])
    authorize @user
    UserMailer.ask_access(current_user, params[:ask_access][:body]).deliver_now
    respond_to do |format|
      format.js { render }
    end
  end

  protected

  def secure_params
    params.require(:user).permit([
      :username, :avatar, :uid, :provider, :first_name, :last_name,
      :location, :state_id, :gender, :birthday])
  end
end
