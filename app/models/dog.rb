class Dog < ActiveRecord::Base
  has_one :pet, as: :animal
  has_many :mixtures, as: :blended, dependent: :destroy

  accepts_nested_attributes_for :mixtures, reject_if: :all_blank, allow_destroy: true

  validates :mixtures, presence: true

  delegate :name, :slug, :description, :color, :weight, :gender, :facility,
           to: :pet, prefix: true
end
