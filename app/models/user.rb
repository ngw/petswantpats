class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook]

  resourcify
  rolify

  extend FriendlyId
  friendly_id :username, use: :slugged

  belongs_to :state

  validates :username, presence: true

  mount_uploader :avatar, AvatarUploader, validate_download: false

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
      extract_from_fb_auth(user, auth)
    end
  end

  def self.extract_from_fb_auth(user, auth)
    user.email = auth.info.email
    user.first_name = auth.info.first_name
    user.last_name = auth.info.last_name
    user.location = auth.info.location
    user.gender = extract_fb_gender(auth)
    user.birthday = extract_fb_birthday(auth)
    user.password = Devise.friendly_token[0, 8]
  end

  def self.extract_fb_gender(auth)
    unless auth.extra.raw_info.gender.nil?
      (auth.extra.raw_info.gender == 'uomo') ? false : true
    end
  end

  def self.extract_fb_birthday(auth)
    unless auth.extra.raw_info.birthday.nil?
      Date.strptime(auth.extra.raw_info.birthday, '%m/%d/%Y')
    end
  end

  def facilities
    if has_role?(:owner, :any)
      roles.where(resource_type: 'Facility').collect(&:resource)
    else
      []
    end
  end
end
