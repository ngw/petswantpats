class Mixture < ActiveRecord::Base
  belongs_to :blended, polymorphic: true
  belongs_to :breed

  attr_accessor :input_name

  validates :breed, presence: true
  validates :breed, uniqueness: { scope: [:blended_id, :blended_type] }
  validate :percentage_cannot_be_more_than_100

  delegate :name, prefix: true, to: :breed

  def input_name
    breed_name if breed
  end

  def input_name=(name)
    self.breed = Breed.find_or_initialize_by(
      name: name.squeeze.strip.downcase.capitalize)
  end

  private

  def partial_percentage
    Mixture.where(
      ['id != ? AND blended_id = ? AND blended_type = ?',
       id, blended_id, blended_type]).select(:percentage)
  end

  def percentage_cannot_be_more_than_100
    total = partial_percentage.sum(:percentage).to_i + percentage.to_i
    return if total <= 100
    errors.add(
      :percentage,
      I18n.t(
        'errors.messages.percentage_sum_less_than', percentage: "#{total}%"))
  end
end
