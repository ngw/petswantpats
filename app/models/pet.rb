class Pet < ActiveRecord::Base
  extend FriendlyId
  friendly_id :breed_and_name, use: [:slugged, :scoped], scope: :facility

  belongs_to :animal, polymorphic: true, dependent: :destroy
  belongs_to :facility
  has_many :pet_pictures, dependent: :destroy

  accepts_nested_attributes_for :animal
  accepts_nested_attributes_for :pet_pictures, reject_if: :all_blank, allow_destroy: true

  alias_attribute :pictures, :pet_pictures
  alias_attribute :pictures=, :pet_pictures=

  validates :name, :birth, :facility, :pet_pictures, presence: true
  validates :color, inclusion: { in: 0..25 }
  validates_associated :facility

  searchkick

  def self.search_params(params)
    attrs = cleanup(params)
    if attrs.key?(:advanced_search)
      advanced_search = cleanup(attrs[:advanced_search])
      advanced_search[:birth] = birth_range(advanced_search[:birth]) if advanced_search[:birth]
      advanced_search[:weight] = weight_range(advanced_search[:weight]) if advanced_search[:weight]
      advanced_search[:breeds] = [advanced_search[:breeds]] if advanced_search[:breeds]
      advanced_search = advanced_search.delete_if { |k, v| k == :heart && v == 'false' }
      { where: advanced_search }.deep_symbolize_keys
    else
      attrs.deep_symbolize_keys[:q]
    end
  end

  def self.cleanup(params)
    params.select { |_k, v| !v.blank? }
  end

  def self.birth_range(age)
    { lte: age.years.ago, gte: (age + 1).years.ago }
  end

  def self.weight_range(weight)
    from, to = weight.split('-')
    to.nil? ?
      { lte: (from.delete('+') + 15).to_i, gte: from.delete('+').to_i } :
      { lte: to.to_i, gte: from.to_i }
  end

  def search_data
    { name: name,
      birth: birth,
      gender: gender,
      color: color,
      weight: weight,
      heart: heart,
      size_at_maturity: animal.size_at_maturity,
      fur_length: animal.fur_length,
      spayed: animal.spayed,
      vaccinated: animal.vaccinated,
      breed: animal.decorate.breed,
      breeds: animal.mixtures.collect { |m| m.breed.id },
      facility: facility.name,
      state: facility.state.name }
  end

  def main_picture
    pictures.where(main: true).first
  end

  def build_animal(params)
    self.animal = Dog.new(params)
  end

  def description=(content)
    if content == '<p><br></p>'
      super('')
    else
      super(content)
    end
  end

  def breed_and_name
    "#{animal.mixtures.collect { |m| m.breed.name }.join(' ')} #{name}"
  end
end
