class Facility < ActiveRecord::Base
  extend FriendlyId
  friendly_id :state_and_city_and_name, use: :slugged

  resourcify

  mount_uploader :logo, FacilityLogoUploader

  belongs_to :state
  has_many :pets, dependent: :destroy

  validates :name, :address, :postal_code, :city, :province, :state, :contact_email,
            presence: true

  geocoded_by :full_address

  after_validation :geocode, if: :addr_changed?

  def full_address
    "#{address}, #{postal_code} #{city} (#{province})"
  end

  def full_address_with_state
    "#{full_address}, #{state.name}"
  end

  def construct_pet(animal)
    model = animal.to_s.capitalize.constantize
    pet = pets.build animal: model.send(:new)
    pet.pet_pictures.build
    pet.animal.mixtures.build
    pet
  end

  private

  def state_and_city_and_name
    "#{state.name} #{city} #{name}"
  end

  def addr_changed?
    full_address.present? && address_changed?
  end
end
