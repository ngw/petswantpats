class State < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :facilities
  has_many :users

  validates :name, :slug, presence: true

  default_scope { order(:name) }
  scope :that_has_facilities, -> { joins(:facilities) }
end
