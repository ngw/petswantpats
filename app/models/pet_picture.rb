class PetPicture < ActiveRecord::Base
  belongs_to :pet

  mount_uploader :photo_file, PetPhotoFileUploader

  validates :photo_file, presence: true

  before_save :set_main_when_unavailable, if: :no_main_picture?
  after_save :only_one_main_picture, if: :main_picture_changed?

  scope :pet_main_picture, ->(pet_id) { where(pet_id: pet_id, main: true) }

  private

  def no_main_picture?
    PetPicture.pet_main_picture(pet_id).empty?
  end

  def set_main_when_unavailable
    self.main = true
  end

  def only_one_main_picture
    PetPicture.pet_main_picture(pet_id).where(
      ['id != :id', { id: id }]).update_all(main: false)
  end

  def main_picture_changed?
    main_changed? && main
  end
end
