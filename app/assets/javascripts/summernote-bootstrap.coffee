$ ->
  # to set summernote object
  summer_note = $('#summernote')

  # to call summernote editor
  summer_note.summernote
    height: 160
    lang: 'it-IT'
    toolbar: [
      [
        'style'
        [
          'bold'
          'italic'
          'underline'
        ]
      ]
      [
        'para'
        [
          'ul'
          'ol'
        ]
      ]
    ]

  # to set code for summernote
  summer_note.code summer_note.val()

  # to get code for summernote
  summer_note.closest('form').submit ->
    # alert $('#post_content').code()
    summer_note.val summer_note.code()
    true
