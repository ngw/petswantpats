$(document).ready(function() {
  $('#order-by').change(function() {
    $.ajax({
      url: window.location.pathname + '.js' + $.param.querystring(window.location.search, {order: $('#order-by option:selected').attr('name')})
    });
  });
});