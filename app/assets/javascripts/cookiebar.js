$(document).ready(function(){
  $.cookieBar({
    'message': "Questo sito utilizza cookie anche di terze parti, per migliorare l'esperienza utente, motivi statistici e pubblicità.",
    'acceptText': 'Accetto',
    'policyButton': true,
    'policyURL': '/privacy'});
});