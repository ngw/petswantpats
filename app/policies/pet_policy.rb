class PetPolicy < ApplicationPolicy
  [:new?, :create?, :destroy?, :edit?, :update?].each do |method|
    define_method method do
      @modifier.is_admin? || @modifier.has_role?(:owner, @modified.facility)
    end
  end
end
