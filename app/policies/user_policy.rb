class UserPolicy < ApplicationPolicy
  [:edit?, :update?, :ask_access?].each do |method|
    define_method method do
      admin_or_self?
    end
  end

  protected

  def admin_or_self?
    @modifier.has_role?(:admin) || @modifier == @modified
  end
end
