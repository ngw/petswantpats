class FacilityPolicy < ApplicationPolicy
  [:new?, :create?, :destroy?].each do |method|
    define_method method do
      @modifier.is_admin?
    end
  end

  [:edit?, :update?].each do |method|
    define_method method do
      @modifier.is_admin? || @modifier.has_role?(:owner, @modified)
    end
  end
end
