class ApplicationPolicy
  attr_reader :modifier, :modified

  def initialize(modifier, modified)
    @modifier = modifier
    @modified = modified
  end
end
