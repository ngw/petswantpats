class UserMailer < ApplicationMailer
  def ask_access(user, body)
    @user = user
    @body = body
    mail(to: 'lola@petswantpats.com',
         from: @user.email,
         subject: "Richiesta di accesso privilegiato da #{@user.email}.")
  end

  def adopt(facility, pet, user, body)
    @facility = facility
    @pet = pet
    @user = user
    @body = body
    mail(to: facility.contact_email,
         from: 'lola@petswantpats.com',
         subject: "Richiesta di adozione per #{@pet.name}")
  end
end
