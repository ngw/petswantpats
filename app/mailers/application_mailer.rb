class ApplicationMailer < ActionMailer::Base
  default from: 'lola@petswantpats.com'
  layout 'mailer'
end
