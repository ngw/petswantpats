# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

[ "Valle d'Aosta", "Piemonte", "Liguria", "Lombardia", "Trentino-Alto Adige", "Veneto", "Friuli-Venezia Giulia", "Emilia-Romagna",
  "Toscana", "Marche", "Umbria", "Lazio", "Abruzzo", "Molise", "Campania", "Basilicata", "Puglia", "Calabria", "Sicilia", "Sardegna"].each do |state_name|
  State.create(name: state_name)
end
