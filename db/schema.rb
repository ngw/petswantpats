# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160110130623) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "breeds", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "slug",        null: false
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "dogs", force: :cascade do |t|
    t.integer  "size_at_maturity", default: 0
    t.boolean  "vaccinated"
    t.boolean  "dewormed"
    t.boolean  "spayed"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "microchipped",     default: false
    t.integer  "fur_length",       default: 0
  end

  create_table "facilities", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "slug",          null: false
    t.string   "address",       null: false
    t.string   "postal_code",   null: false
    t.string   "city",          null: false
    t.string   "province",      null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "logo"
    t.text     "description"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "telephone"
    t.integer  "state_id"
    t.string   "contact_email"
  end

  add_index "facilities", ["slug"], name: "index_facilities_on_slug", unique: true, using: :btree
  add_index "facilities", ["state_id"], name: "index_facilities_on_state_id", using: :btree

  create_table "mixtures", force: :cascade do |t|
    t.integer  "percentage"
    t.integer  "blended_id",   null: false
    t.string   "blended_type", null: false
    t.integer  "breed_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "mixtures", ["blended_id", "blended_type"], name: "index_mixtures_on_blended_id_and_blended_type", using: :btree
  add_index "mixtures", ["breed_id"], name: "index_mixtures_on_breed_id", using: :btree

  create_table "pet_pictures", force: :cascade do |t|
    t.string   "photo_file",                 null: false
    t.string   "caption"
    t.integer  "pet_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "main",       default: false, null: false
  end

  add_index "pet_pictures", ["pet_id"], name: "index_pet_pictures_on_pet_id", using: :btree

  create_table "pets", force: :cascade do |t|
    t.string   "name",                        null: false
    t.date     "birth",                       null: false
    t.boolean  "gender",      default: true,  null: false
    t.integer  "color"
    t.integer  "weight"
    t.text     "description"
    t.integer  "animal_id"
    t.string   "animal_type"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "facility_id"
    t.string   "slug",                        null: false
    t.boolean  "heart",       default: false
  end

  add_index "pets", ["animal_id", "animal_type"], name: "index_pets_on_animal_id_and_animal_type", using: :btree
  add_index "pets", ["facility_id"], name: "index_pets_on_facility_id", using: :btree
  add_index "pets", ["slug"], name: "index_pets_on_slug", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "states", force: :cascade do |t|
    t.string "name", null: false
    t.string "slug", null: false
  end

  add_index "states", ["slug"], name: "index_states_on_slug", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "username",                              null: false
    t.string   "slug",                                  null: false
    t.string   "avatar"
    t.string   "provider"
    t.string   "uid"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "location"
    t.integer  "state_id"
    t.boolean  "gender",                 default: true, null: false
    t.date     "birthday"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["provider"], name: "index_users_on_provider", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", using: :btree
  add_index "users", ["state_id"], name: "index_users_on_state_id", using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
