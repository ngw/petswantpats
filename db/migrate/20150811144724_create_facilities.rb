class CreateFacilities < ActiveRecord::Migration
  def change
    create_table :facilities do |t|
      t.string :name, null: false
      t.string :slug, null: false
      t.string :address, null: false
      t.string :postal_code, null: false
      t.string :city, null: false
      t.string :province, null: false
      t.string :region, null: false
      t.timestamps null: false
    end
    add_index :facilities, :slug, unique: true
  end
end
