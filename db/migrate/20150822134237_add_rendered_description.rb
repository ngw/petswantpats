class AddRenderedDescription < ActiveRecord::Migration
  def change
    add_column :facilities, :rendered_description, :text
  end
end
