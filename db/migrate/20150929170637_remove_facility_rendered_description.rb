class RemoveFacilityRenderedDescription < ActiveRecord::Migration
  def change
    remove_column :facilities, :rendered_description
  end
end
