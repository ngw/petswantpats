class CreateBreeds < ActiveRecord::Migration
  def change
    add_column :dogs, :microchipped, :boolean, default: false
    create_table :breeds do |t|
      t.string :name, null: false
      t.string :slug, null: false
      t.text :description, default: nil
      t.timestamps null: false
    end
  end
end
