class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
    	t.string :photo_file, null: false
    	t.string :caption
    	t.references :pet, index: true
      t.timestamps null: false
    end
  end
end
