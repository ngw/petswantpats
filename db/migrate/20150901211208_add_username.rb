class AddUsername < ActiveRecord::Migration
  def change
    add_column :users, :username, :string, null: false
    add_column :users, :slug, :string, null: false
    add_index :users, :slug
  end
end
