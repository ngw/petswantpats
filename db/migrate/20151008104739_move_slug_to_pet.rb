class MoveSlugToPet < ActiveRecord::Migration
  def change
    add_column :pets, :slug, :string
    add_index :pets, :slug
    Dog.all.each do |dog|
      dog.pet.update_attribute(:slug, dog.slug)
    end
    remove_index :dogs, :slug
    remove_column :dogs, :slug
    change_column :pets, :slug, :string, null: false
  end
end
