class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
    	t.string :name, null: false
      t.datetime :birth, null: false
      t.boolean :gender, null: false, default: true
      t.integer :color
      t.integer :weight
    	t.text :description
    	t.integer :animal_id
    	t.string :animal_type
      t.timestamps null: false
    end
    add_index :pets, [:animal_id, :animal_type]
  end
end
