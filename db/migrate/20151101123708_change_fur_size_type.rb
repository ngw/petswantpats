class ChangeFurSizeType < ActiveRecord::Migration
  def change
  	remove_column :dogs, :fur_size
	  add_column :dogs, :fur_length, :boolean, null: false, default: false
  end
end
