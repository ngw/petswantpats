class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :name, null: false
      t.string :slug, null: false
    end
    add_reference :facilities, :state, index: true
    add_index :states, :slug, unique: true
  end
end
