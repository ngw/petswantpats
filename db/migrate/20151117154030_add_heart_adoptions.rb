class AddHeartAdoptions < ActiveRecord::Migration
  def change
    add_column :pets, :heart, :boolean, default: false
  end
end
