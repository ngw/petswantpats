class RemoveRegion < ActiveRecord::Migration
  def change
    remove_column :facilities, :region, :string
  end
end
