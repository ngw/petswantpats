class AddPicturePriority < ActiveRecord::Migration
  def change
    add_column :pictures, :main, :boolean, default: false, null: false
  end
end
