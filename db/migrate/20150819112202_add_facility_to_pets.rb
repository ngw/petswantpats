class AddFacilityToPets < ActiveRecord::Migration
  def change
    add_reference :pets, :facility, index: true
  end
end
