class ChangeFurLength < ActiveRecord::Migration
  def change
    remove_column :dogs, :fur_length
    add_column :dogs, :fur_length, :integer, default: 0
  end
end
