class AddBdAndGender < ActiveRecord::Migration
  def change
    add_column :users, :gender, :boolean, null: false, default: true
    add_column :users, :birthday, :date
  end
end
