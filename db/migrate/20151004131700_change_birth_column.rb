class ChangeBirthColumn < ActiveRecord::Migration
  def self.up
    change_column :pets, :birth, :date, null: false
  end

  def self.down
    change_column :pets, :birth, :datetime
  end
end
