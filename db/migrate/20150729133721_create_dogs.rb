class CreateDogs < ActiveRecord::Migration
  def change
    create_table :dogs do |t|
      t.string :slug, null: false
      t.integer :fur_size, default: 0
      t.integer :size_at_maturity, default: 0
      t.boolean :vaccinated
      t.boolean :dewormed
      t.boolean :spayed
      t.timestamps null: false
    end
    add_index :dogs, :slug
  end
end
