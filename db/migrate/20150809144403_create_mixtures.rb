class CreateMixtures < ActiveRecord::Migration
  def change
    create_table :mixtures do |t|
      t.integer :percentage, null: true
      t.integer :blended_id, null: false
      t.string :blended_type, null: false
      t.references :breed, index: true
      t.timestamps null: false
    end
    add_index :mixtures, [:blended_id, :blended_type]
  end
end
