class AddFacilityDescriptionAndGeoloc < ActiveRecord::Migration
  def change
    change_table :facilities do |t|
      t.text :description
      t.float :latitude
      t.float :longitude
    end
  end
end
