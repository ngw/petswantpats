class RenamePictureTable < ActiveRecord::Migration
  def change
    rename_table :pictures, :pet_pictures
  end
end
