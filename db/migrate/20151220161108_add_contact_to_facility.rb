class AddContactToFacility < ActiveRecord::Migration
  def change
    add_column :facilities, :contact_email, :string
    Facility.all.each do |facility|
      facility.contact_email = 'info@example.com'
      facility.destroy if !facility.save
    end
  end
end
