SitemapGenerator::Sitemap.default_host = "http://petswantpats.com"
SitemapGenerator::Sitemap.create_index = true
SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'
SitemapGenerator::Sitemap.sitemaps_host = 'http://petswantpats.com'

SitemapGenerator::Sitemap.create do
  add root_path, changefreq: 'monthly', priority: 0.6
  add terms_path, changefreq: 'monthly', priority: 0.6
  add privacy_path, changefreq: 'monthly', priority: 0.6
  add contacts_path, changefreq: 'monthly', priority: 0.6

  State.that_has_facilities.each do |state|
    add state_path(state.slug), changefreq: 'weekly', priority: 0.7
  end

  Facility.all.each do |facility|
    add facility_path(facility.slug), changefreq: 'monthly', priority: 0.8
  end

  Pet.all.each do |pet|
    add facility_pet_path(facility_id: pet.facility.slug, id: pet.slug), changefreq: 'weekly', priority: 0.9
  end
end
