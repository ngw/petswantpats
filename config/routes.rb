Rails.application.routes.draw do
  devise_for :users,  controllers:  { registrations: 'users/registrations',
                                      omniauth_callbacks: 'users/callbacks' },
                      skip:         [:sessions]
  as :user do
    get '/users/sign_up',      to: 'devise/sessions#new',            as: :new_user_session
    get '/users/sign_out',     to: 'devise/sessions#destroy'
    post '/users/sign_in',     to: 'devise/sessions#create',         as: :user_session
    delete '/users/sign_out',  to: 'devise/sessions#destroy',        as: :destroy_user_session
    get '/users/signed_up',    to: 'users/registrations#signed_up',  as: :signed_up
  end

  resources :users, only: [:edit, :update] do
    post :ask_access
  end

  resources :facilities, only: [:show, :edit, :update] do
    resources :pets, only: [:new, :create, :edit, :update, :show] do
      post :adopt
    end
  end

  resources :states, only: [:show]

  get 'pets/search', to: 'pets#search'
  get 'pets/advanced_search_form', to: 'pets#advanced_search_form'

  get 'terms', to: 'statics#terms'
  get 'privacy', to: 'statics#privacy'
  get 'contacts', to: 'statics#contacts'

  root 'pets#index'
end
