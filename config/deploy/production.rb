role :web,                        %w{deployer@46.101.114.182}
role :app,                        %w{deployer@46.101.114.182}
role :db,                         %w{deployer@46.101.114.182}

set :stage,                       'production'
set :rails_env,                   'production'
set :branch,                      ENV['BRANCH'] || 'master'

set :puma_access_log,             "#{shared_path}/log/puma_error.log"
set :puma_error_log,              "#{shared_path}/log/puma_access.log"
set :puma_conf,                   "#{shared_path}/config/puma.rb"
set :puma_state,                  "#{shared_path}/tmp/sockets/puma.state"
set :puma_bind,                   "#{shared_path}/tmp/sockets/puma.sock"
