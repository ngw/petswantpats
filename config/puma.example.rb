#!/usr/bin/env puma

directory '/home/deployer/apps/petswantpats/current'
rackup "/home/deployer/apps/petswantpats/current/config.ru"
environment 'production'

pidfile "/home/deployer/apps/petswantpats/shared/tmp/pids/puma.pid"
state_path "/home/deployer/apps/petswantpats/shared/tmp/sockets/puma.state"
stdout_redirect '/home/deployer/apps/petswantpats/shared/log/puma_error.log',
                '/home/deployer/apps/petswantpats/shared/log/puma_access.log', true

threads 0,16

bind "unix:///home/deployer/apps/petswantpats/shared/tmp/sockets/puma.sock"
workers 3

preload_app!

on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = "/home/deployer/apps/petswantpats/current/Gemfile"
end

on_worker_boot do |wid|
  File.open("/home/deployer/apps/petswantpats/shared/tmp/pids/puma_#{wid}.pid", "w") do |f|
    f.puts Process.pid
  end
end
