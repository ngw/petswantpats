Feature: Manage a pet
  In order for a pet to be correctly represented
  A user
  Should be able to edit it

  @javascript
  Scenario: Edit a pet
    Given I am logged in
    And I am an owner of a facility
    And I've added a pet
    When I visit the admin section of my facility
    And I edit a pet
    And I change the pet data
    Then I should see the pet edited
