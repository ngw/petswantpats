Feature: Manage a pet
  In order for a pet to exist
  A user
  Should be able to manage it

  @javascript
  Scenario: Add a pet
    Given I am logged in
    And I am an owner of a facility
    When I visit the admin section of my facility
    And I add a pet
    And I insert the new pet's data
    Then I should see the pet
