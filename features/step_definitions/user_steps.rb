def create_visitor
  @visitor ||= { username: 'johndoe', email: 'example@example.com',
                 password: 'changeme', password_confirmation: 'changeme' }
end

def create_user
  create_visitor
  delete_user
  @user = FactoryGirl.create(:johndoe, @visitor.merge(confirmed_at: Time.now))
end

def delete_user
  @user ||= User.where(email: @visitor[:email]).first
  @user.destroy unless @user.nil?
end

def find_user
  @user ||= User.where(email: @visitor[:email]).first
end

def sign_up
  visit '/users/sign_up'
  fill_in 'username', with: @visitor[:username]
  fill_in 'email', with: @visitor[:email]
  fill_in 'password', with: @visitor[:password]
  fill_in 'password_confirmation', with: @visitor[:password_confirmation]
  click_button 'Registrati'
  find_user
end

def sign_in
  visit '/users/sign_up'
  fill_in 'login_email', with: @visitor[:email]
  fill_in 'login_password', with: @visitor[:password]
  click_button 'Accedi'
end

### GIVEN ###
Given /^I am not logged in$/ do
  visit '/users/sign_out'
end

Given /^I do not exist as a user$/ do
  create_visitor
  delete_user
end

Given /^I exist as a user$/ do
  create_user
end

Given /^I am logged in$/ do
  create_user
  sign_in
end

### WHEN ###
When /^I sign up with valid user data$/ do
  create_visitor
  sign_up
end

When /^I sign up with an invalid email$/ do
  create_visitor
  @visitor = @visitor.merge(email: 'notanemail')
  sign_up
end

When /^I sign up without a password$/ do
  create_visitor
  @visitor = @visitor.merge(password: '')
  sign_up
end

When /^I sign up without a password confirmation$/ do
  create_visitor
  @visitor = @visitor.merge(password_confirmation: '')
  sign_up
end

When /^I sign up with a mismatched password confirmation$/ do
  create_visitor
  @visitor = @visitor.merge(password_confirmation: 'changeme123')
  sign_up
end

When /^I sign in with valid credentials$/ do
  create_visitor
  sign_in
end

When /^I return to the site$/ do
  visit '/'
end

When /^I sign in with a wrong email$/ do
  @visitor = @visitor.merge(email: 'wrong@example.com')
  sign_in
end

When /^I sign in with a wrong password$/ do
  @visitor = @visitor.merge(password: 'wrongpass')
  sign_in
end

When /^I sign out$/ do
  visit '/users/sign_out'
end

When /^I edit my account details$/ do
  click_link 'johndoe'
  fill_in 'user_username', with: 'newname'
  click_button 'Aggiorna'
end

When /^I visit my account details page$/ do
  click_link 'johndoe'
end

### THEN ###
Then /^I should see a successful sign up message$/ do
  expect(page).to have_content('La registrazione è quasi completa.')
end

Then /^I should see an invalid email message$/ do
  expect(page).to have_content('Indirizzo E-mail non è valido')
end

Then /^I should see a missing password message$/ do
  text = <<-END
  Il campo password non può essere lasciato in bianco
  END
  expect(page).to have_content(text)
end

Then /^I should see a missing password confirmation message$/ do
  expect(page).to have_content('Il campo password non coincide con la conferma')
end

Then /^I should see a mismatched password message$/ do
  expect(page).to have_content('Il campo password non coincide con la conferma')
end

Then /^I see an invalid login message$/ do
  expect(page).to have_content('Indirizzo email o password non validi.')
end

Then /^I should be signed out$/ do
  expect(page).to have_content('Registrati/Accedi')
  expect(page).not_to have_content('Logout')
end

Then /^I see a successful sign in message$/ do
  expect(page).to have_content('Accesso effettuato con successo.')
end

Then /^I should be signed in$/ do
  expect(page).to have_content('Esci')
  expect(page).not_to have_content('Registrati/Accedi')
end

Then /^I should see a signed out message$/ do
  expect(page).to have_content('Sei uscito correttamente.')
end

Then /^I should see an account edited message$/ do
  expect(page).to have_content('Il tuo account è stato aggiornato.')
end
