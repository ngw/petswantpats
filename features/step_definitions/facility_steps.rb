def create_facility
  @facility = FactoryGirl.create(:lida, state: FactoryGirl.create(:sardegna))
end

## GIVEN ##
Given(/^I am not an owner of any facility$/) do
  expect(@user.has_role?(:owner, :any)).to eq(false)
end

Given(/^I am an owner of a facility$/) do
  create_facility
  @user.add_role(:owner, @facility)
  expect(@user.has_role?(:owner, @facility)).to be_truthy
end

### WHEN ###
When(/^attempt to add a facility$/) do
  find(:linkhref, '#facility').click
end

When(/^edit that facility$/) do
  click_link "Rifugio \"I Fratelli Minori\""
  fill_in 'facility_name', with: 'Nuovo nome di test'
  fill_in 'facility[description]', with: 'Descrizione di test'
  click_button 'update_facility'
end

### THEN ###
Then(/^I should be informed on how to ask permission$/) do
  text = <<-END
  Prendiamo PetsWantPats e i nostri obblighi nei confronti dei nostri amici
  a 4 zampe con grandissima serietà
  END
  expect(page).to have_content(text)
end

Then(/^I can ask for a facility$/) do
  click_button('Richiedi un accesso privilegiato!')
  text = 'Ricordati di includere il nome della struttura'
  expect(page).to have_content(text)
end

Then(/^I should see it updated$/) do
  expect(page).to have_content('Nuovo nome di test')
  expect(page).to have_content('Descrizione di test')
end
