def create_pet
  facility = Facility.first
  if facility
    @dog = FactoryGirl.create(:dog,
                              animal: FactoryGirl.create(:marley,
                                                         mixtures: [
                                                           FactoryGirl.build(
                                                             :marley_yorkshire,
                                                             breed: FactoryGirl.create(
                                                               :yorkshire))]),
                              facility: facility,
                              pet_pictures: [FactoryGirl.create(:marleys_pic),
                                             FactoryGirl.create(:second_marleys_pic)])
  else
    @dog = FactoryGirl.create(:dog,
                              animal: FactoryGirl.create(
                                :marley,
                                mixtures: [
                                  FactoryGirl.build(
                                    :marley_yorkshire, breed: FactoryGirl.create(:yorkshire))]),
                              facility: FactoryGirl.create(
                                :lida, state: FactoryGirl.create(:sardegna)),
                              pet_pictures: [FactoryGirl.create(:marleys_pic)])
  end
end

### GIVEN ###
Given(/^I've added a pet$/) do
  create_pet
end

### WHEN ###
When(/^I visit the admin section of my facility$/) do
  step 'I visit my account details page'
  click_link 'Rifugio "I Fratelli Minori"'
  click_link 'Gestisci gli ospiti della struttura.'
end

When(/^I add a pet$/) do
  click_link 'Aggiungi un cane'
end

When(/^I insert the new pet's data$/) do
  fill_in :pet_name, with: 'Test dog'
  select '5', from: 'pet_birth_3i'
  select 'agosto', from: 'pet_birth_2i'
  select '2013', from: 'pet_birth_1i'
  select 'Femmina', from: 'Sesso'
  select 'Oro', from: 'pet_color'
  fill_in :pet_weight, with: '10'
  select 'Lungo', from: 'pet_animal_attributes_fur_length'
  click_link 'Aggiungi immagine'
  first('input.fileinput').set(
    File.join(Rails.root, '/spec/fixtures/example.jpg'))
  first('.pet_animal_mixtures_input_name input').set('Dachsund')
  click_button 'Aggiungi'
end

When(/^I edit a pet$/) do
  click_link('Marley')
  click_link('Gestisci')
end

When(/^I change the pet data$/) do
  fill_in :pet_name, with: 'Test dog'
  select 'Maschio', from: 'Sesso'
  select 'Corto', from: 'pet_animal_attributes_fur_length'
  click_button 'Aggiorna'
end

### THEN ###
Then(/^I should see the pet$/) do
  expect(page).to have_content('Test dog')
  expect(page).to have_content('Oro')
  expect(page).to have_content('Femmina')
end

Then(/^I should see the pet edited$/) do
  expect(page).to have_content('Test dog')
  expect(page).to have_content('Maschio')
  expect(page).to have_content('Corto')
end
