Feature: Manage a facility
  In order for a facility to exist
  A user
  Should be able to manage it

  Scenario: User is not an owner
    Given I am logged in
    And I am not an owner of any facility
    When I visit my account details page
    And attempt to add a facility
    Then I should be informed on how to ask permission
    And I can ask for a facility

  Scenario: User updates a facility
    Given I am logged in
    And I am an owner of a facility
    When I visit my account details page
    And edit that facility
    Then I should see it updated
