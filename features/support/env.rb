require 'cucumber/rails'

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

ActionController::Base.allow_rescue = false

begin
  DatabaseCleaner.strategy = :transaction
rescue NameError
  raise 'You\'re missing the database_cleaner gem.'
end

VCR.configure do |c|
  c.ignore_localhost = true
end

Cucumber::Rails::Database.javascript_strategy = :truncation
