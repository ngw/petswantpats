source 'https://rubygems.org'

gem 'rails'
gem 'pg'
gem 'dotenv-rails'
gem 'draper'
gem 'friendly_id'
gem 'uglifier'
gem 'coffee-rails'
gem 'therubyracer', platforms: :ruby
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder'
gem 'sass-rails'
gem 'bootstrap-sass'
gem 'font-awesome-rails'
gem 'summernote-rails', '0.6.16'
gem 'carrierwave-aws'
gem 'carrierwave'
gem 'mini_magick'
gem 'geocoder'
gem 'haml'
gem 'haml-rails'
gem 'metamagic'
gem 'canonical-rails'
gem 'sitemap_generator'
gem 'simple_form'
gem 'cocoon'
gem 'kaminari'
gem 'puma'

# Auth and permissions
gem 'devise'
gem 'omniauth-oauth2', '1.3.1'
gem 'omniauth-facebook'
gem 'pundit'
gem 'rolify'

# Search
gem 'searchkick'

group :development, :test do
  gem 'capybara'
  gem 'capybara-webkit'
  gem 'byebug'
  gem 'spring'
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'vcr'
end

group :development do
  gem 'pry-rails'
  gem 'rubocop'
  gem 'web-console'
  gem 'rails_best_practices', require: false
  gem 'guard'
  gem 'guard-rspec', require: false
  gem 'guard-cucumber', require: false

  # Deploy
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano3-puma'
  gem 'capistrano-ssh-doctor'
end

group :test do
  gem 'cucumber-rails', require: false
  gem 'simplecov', require: false
  gem 'database_cleaner'
  gem 'webmock'
  gem 'fuubar'
  gem 'fuubar-cucumber'
  gem 'launchy'
end
